<!DOCTYPE html>
<html lang="fr">
<meta charset="UTF-8">

<head>
    <script src="../js/main.js"></script>
    <link rel="shortcut icon" type="image/jpg" href="img/favicon.png"/>
    <link rel="stylesheet" href="css/form.css">
    <title>Hall of Fame | Formulaire</title>
</head>
<body>
    <div class="container">
        <div class="left">
            <div class="header">
                <h2 class="animation a1">Nouveau contrat&nbsp;!</h2>
                <h4 class="animation a2">
                    <script>congratsText();</script><br />
                    Entrez vos informations ci-dessous.</h4>
            </div>
            
            <form class="form" method="post" action="confirmation.php">
            
                <input type="text" required id="manager" name="manager" class="form-field animation a3" placeholder="Votre nom (Manager)">
                <input type="text" required id="client" name="client" class="form-field animation a3" placeholder="Entreprise Cliente">
                <input type="text" required id="consultant" name="consultant" class="form-field animation a4" placeholder="Consultant">
                <input type="text" required id="hr" name="hr" list="suggestionHr" class="form-field animation a4" placeholder="Recruteur">

                <div id="addImagesButton" onclick="toggleImageFields()">&#9655; <i>Add images?</i></div>

                <div id="addImages">
                    <input type="url" id="managerPic" name="managerPic" class="form-field animation a1" placeholder="(facultatif) Lien vers photo manager">
                    <input type="url" id="consultantPic" name="consultantPic" class="form-field animation a1" placeholder="(facultatif) Lien vers photo consultant">
                </div>

            <!-- datalist id="suggestionHr">
                    <option value="Anne ÉMONNE">Anne ÉMONNE</option>
                    <option value="Bob INAGE">Bob INAGE</option>
                    <option value="Charles ATTAN">Charles ATTAN</option>
                    <option value="Denis DOISEAUX">Denis DOISEAUX</option>
                    <option value="Éva CUATION">Éva CUATION</option>
                    <option value="Fanny HON">Fanny HON</option>
                </datalist>-->

                <div class="form-checkboxes">
                    <div class="form-checkbox">
                        <input type="checkbox" class="fancy-check animation a5" id="confetti" name="confetti" value="doTheAnimation" checked></input>
                        <label class="fancy-check-label animation a5" for="confetti">
                            <abbr title="Toutes les 15 minutes, des confettis seront lancés. Attention, il peut y avoir un peu de latence !">Lancer des confettis</abbr> ?
                        </label>
                    </div>

                    <div class="form-checkbox">
                        <input type="checkbox" class="fancy-check animation a5" id="changeColor" name="changeColor" value="0" checked></input>
                        <label class="fancy-check-label animation a5" for="changeColor">
                            <abbr title="Pendant les 15 premières minutes, la couleur du texte changera, attirant sûrement l'attention">Changer la couleur du texte</abbr> ?
                        </label>
                    </div>

                    <div class="form-checkbox">
                        <input type="checkbox" class="fancy-check animation a5" id="soundAtStart" name="soundAtStart" value="doTheAnimation"></input>
                        <label class="fancy-check-label animation a5" for="soundAtStart">
                            <abbr title="Jouer un son (générique de Kaamelott) dès que la page est chargée">Appeler un orchestre</abbr> ?
                        </label>
                    </div>

                    <div class="form-checkbox">
                        <input type="checkbox" class="fancy-check animation a5" id="animationAtStart" name="animationAtStart" value="doTheAnimation"></input>
                        <label class="fancy-check-label animation a5" for="animationAtStart">
                        <abbr title="Secouer la page une fois pendant une seconde. Attention, c'est très... remarquable !">Animer la page</abbr> ? 
                        </label>
                    </div>
                </div> <!-- class="form-checkboxes" -->

                <input type="submit" class="animation a6" value="Envoyer" />
            </form>
        </div> <!-- Class left -->
        
        <div class="right-form"></div>
        </div>
    </div>
</body>
