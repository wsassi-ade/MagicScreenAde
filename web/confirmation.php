<!DOCTYPE html>
<html lang="fr">
<meta charset="UTF-8">

<head>
    <script src="../js/main.js"></script>
    <link rel="shortcut icon" type="image/jpg" href="img/favicon.png"/>
    <link rel="stylesheet" href="css/form.css">
    <title>Hall of Fame | Confirmation</title>

    <?php
        // create conf folder for the first time
        if (!file_exists('conf/counterVisitsLocal'))
        {
            mkdir('conf/', 0777, true);
        }

        $counterFile = fopen('conf/counterVisitsLocal', 'r+');
        $counterVisitsLoc = trim(fgets($counterFile));
        $counterVisitsLoc = $counterVisitsLoc + 1;
        fseek($counterFile, 0);
        fputs($counterFile, $counterVisitsLoc);
        fclose($counterFile);

        // create global conf folder for the first time
        if (!file_exists('conf/sync/counterVisitsGlobal'))
        {
            mkdir('conf/sync/', 0777, true);
        }

        $counterFile = fopen('conf/sync/counterVisitsGlobal', 'r+');
        $counterVisitsGlo = trim(fgets($counterFile));
        $counterVisitsGlo = trim($counterVisitsGlo) + 1;
        fseek($counterFile, 0);
        fputs($counterFile, $counterVisitsGlo);
        fclose($counterFile);
    ?>
</head>

<body>
    <div class="container">
        <div class="left">
            <div class="header">
                <h2 class="animation a1">Formulaire : Confirmation</h2>
                <div class="recap animation a1">
                    <?php 
                        // Update the file (=overwrite old data on it) with latest values from the form
                        if (isset($_POST['manager']) && isset($_POST['client']) && isset($_POST['consultant']) && isset($_POST['hr']))
                        {
                            $dataFile = fopen('conf/sync/dataToShow', 'w+'); // File with the information.
                            fseek($dataFile, 0);

                            fwrite($dataFile, htmlspecialchars($_POST['manager']).PHP_EOL);

                            if (isset($_POST['managerPic']))
                            {
                                fwrite($dataFile, htmlspecialchars($_POST['managerPic']).PHP_EOL);
                            }
                            else
                            {
                                fwrite($dataFile, "nope".PHP_EOL);
                            }

                            fwrite($dataFile, htmlspecialchars($_POST['client']).PHP_EOL);

                            fwrite($dataFile, htmlspecialchars($_POST['consultant']).PHP_EOL);

                            if (isset($_POST['consultantPic']))
                            {
                                fwrite($dataFile, htmlspecialchars($_POST['consultantPic']).PHP_EOL);
                            }
                            else
                            {
                                fwrite($dataFile, "nope".PHP_EOL);
                            }

                            fwrite($dataFile, htmlspecialchars($_POST['hr']).PHP_EOL);

                            fclose($dataFile);

                            /////////////////////////////////////////////

                            $countFile = fopen('conf/sync/showTimeCounters_initialValues', 'w+');
                            fseek($countFile, 0);
                            /* The next 4 if/else statements: gather results from checkboxes */
                            if (isset($_POST['confetti']))
                            {
                                fwrite($countFile, htmlspecialchars($_POST['confetti']).PHP_EOL);
                            }
                            else
                            {
                                fwrite($countFile, "nope".PHP_EOL);
                            }
                            
                            if (isset($_POST['changeColor']))
                            {
                                fwrite($countFile, htmlspecialchars($_POST['changeColor']).PHP_EOL);
                            }
                            else
                            {
                                fwrite($countFile, "nope".PHP_EOL);
                            }
                            
                            if (isset($_POST['soundAtStart']))
                            {
                                fwrite($countFile, htmlspecialchars($_POST['soundAtStart']).PHP_EOL);
                            }
                            else
                            {
                                fwrite($countFile, "nope".PHP_EOL);
                            }
                            
                            if (isset($_POST['animationAtStart']))
                            {
                                fwrite($countFile, htmlspecialchars($_POST['animationAtStart']).PHP_EOL);
                            }
                            else
                            {
                                fwrite($countFile, "nope".PHP_EOL);
                            }
                            
                            fclose($countFile);

                            echo("<h3>Récapitulatif</h3>");
                            echo("<b>" . htmlspecialchars($_POST['manager']) . "</b> ");
                            if (strlen($_POST['managerPic']) > 4)
                            {
                                echo("(<a href='". $_POST['consultantPic'] . "'>photo)</a>) ");
                            }
                            echo("a trouvé une mission chez <b>" . htmlspecialchars($_POST['client']) . "</b>.<br />");
                            echo("<b>" . htmlspecialchars($_POST['consultant']) . "</b> aura la chance de travailler dessus. <br /> Ce consultant ");
                            if (strlen($_POST['consultantPic']) > 4)
                            {
                                echo("(<a href='". $_POST['consultantPic'] . "'>photo)</a>) ");
                            }
                            echo("a été embauché grâce à son talent et à <b>" . htmlspecialchars($_POST['hr']) . "</b>. <br />");

                            if (isset($_POST['confetti']) || isset($_POST['changeColor']) || isset($_POST['soundAtStart']) || isset($_POST['animationAtStart']))
                            {
                                echo("Pour fêter ceci, nous allons ");
                                if (isset($_POST['confetti']))
                                {
                                    echo("préparer des &#x1F38A; <b>confettis</b> et ");
                                }
                                if (isset($_POST['changeColor']))
                                {
                                    echo("changer quelques &#x1F36D; <b>couleurs</b> de la page et ");
                                }
                                if (isset($_POST['soundAtStart']))
                                {
                                    echo("appeler un &#x1F4EF; <b>orchestre</b> et ");
                                }
                                if (isset($_POST['animationAtStart']))
                                {
                                    echo("faire &#x1F92A; <b>bouger</b> les choses et ");
                                }
                                echo("... voilà&nbsp;!");
                            }
                        }
                        else
                        {
                            // Missing fields!
                            echo("<style='color:red';>Vous n'avez pas tout complété ! Les précédentes valeurs seront gardées !</style>");
                        }
                    ?>
                </div>
            </div>

            <div class="info animation a2">
                Les informations envoyées ont bien été enregistrées. Les données seront mises à jour dans moins d'une minute.<br />
                Vous pouvez avoir un aperçu du résultat <a href="showTime.php">ici</a>.
                Une erreur ? Vous pouvez <a href="index.php">recommencer votre saisie ici</a>.<br />
                <i>Ce formulaire a été rempli <?php echo($counterVisitsLoc) ?> fois dans cette agence et <?php echo($counterVisitsGlo) ?> fois globalement.</i><br />
            </div>

            <div class="thanks animation a3">
                <h3>Remerciements</h3>
                Les images ont été gracieusement fournies par :
                <ul>
                    <li><a href="https://pixabay.com/fr/users/capri23auto-1767157/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3100563">Capri23auto</a> de <a href="https://pixabay.com/fr/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3100563">Pixabay</a></li>
                    <li><a href="https://pixabay.com/fr/users/nck_gsl-3554748/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1953253">nck_gsl</a> de <a href="https://pixabay.com/fr/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1953253">Pixabay</a></li>
                </ul>
                La réalisation des feuilles de style de ces pages a été bien plus simple grâce à :
                <ul>
                    <li><a href="https://codepen.io/knyttneve/pen/JQppEw">Mert Cukuren</a> pour le formulaire</li>
                    <li><a href="https://codepen.io/TommiTikall/pen/KymYBN">Tómas Thorvardarson</a> pour les cases à cocher</li>
                    <li><a href="https://thebestmotherfucking.website/">Denys Vitali</a> pour le formatage des pages Web (et pour les lignes de JS avec les couleurs variables)</li>
                    <li>La documentation hyper complète de <a href="https://developer.mozilla.org/fr/docs/Learn/CSS">MDN</a> et de <a href="https://www.w3schools.com/css/default.asp">W3C</a></li>
                </ul>
            </div>
        </div> <!-- Class left -->
        
        <div class="right-confirmation"></div>
        </div>
    </div>
</body>
