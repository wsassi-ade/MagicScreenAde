function congratsText() {
    var congratsText = new Array ();
    var nbTxt = 10;
    congratsText[0] = "You did it!";
    congratsText[1] = "Et un pas de plus vers le succès&nbsp;!";
    congratsText[2] = "Félicitations&nbsp;!";
    congratsText[3] = "C'est votre moment de gloire&nbsp;! Partagez-le&nbsp;!";
    congratsText[4] = "Et un de plus&nbsp;! Bravo&nbsp;!";
    congratsText[5] = "Oh oui&nbsp;!";
    congratsText[6] = "Well played!";
    congratsText[7] = "Super&nbsp;!";
    congratsText[8] = "Fantastique&nbsp;!";
    congratsText[9] = "Youpi&nbsp;! Dansons la carioca &#x1F3B5;";

    var i = Math.floor(nbTxt*Math.random());

    var txt = congratsText[i];

    document.write(txt)
}

function toggleImageFields() {
    var x = document.getElementById("addImages");
    var button = document.getElementById("addImagesButton");
    /* By default, display is null or none (in all cases: not flex) */
    if (x.style.display !== "flex")
    {
        x.style.display = "flex";
        x.style.flexDirection = "column";
        x.style.flexWrap = "wrap";
        button.innerHTML = "&#9661; <i>Hide images field?</i>";
        button.style.color = "#a0a0a0";
    } 
    else
    {
        x.style.display = "none";
        button.innerHTML = "&#9655; <i>Add images?</i>";
        button.style.color = "#808080";
    }
}

function doGreatFireworks( ) {
    var end = Date.now() + (10 * 1000);
    var timeBetweenLaunches = 2000; // duration between two confetti launches (ms)
    var lastSequence = 0;
    var colors = ['#002141', '#ffffff'];

    (function frame() {
    confetti({
        particleCount: 80,
        angle: 0,
        spread: 180,
        origin: { x: 0, y: 0.1},
        colors: colors
    });
    confetti({
        particleCount: 80,
        angle: 180,
        spread: 180,
        origin: { x: 1, y: 0.1},
        colors: colors
    });
    confetti({
        particleCount: 160,
        angle: 270,
        spread: 220,
        origin: { x: 0.5, y: 0},
        colors: colors
    });

    if ((Date.now() < end) && (lastSequence + timeBetweenLaunches < Date.now())) {
        /* Run requestAnimation every second (go easy on device!) */
        setTimeout( function() { requestAnimationFrame(frame);}, timeBetweenLaunches);
    }
    }());
}

// Thank you again @Denys Vitali (https://thebestmotherfucking.website/) 
"use strict";

var currentHue = 0;
var hueAddition = 5;
var rainbowTiming = 1000 / 25;

function rainbowColor() {
    var rTitle = document.getElementsByClassName("rainbowColor")[0];
    var rSubTitle = document.getElementsByClassName("rainbowColor")[1];
    var color = "hsl(" + currentHue + ", 80%, 60%)", nextHue = currentHue + hueAddition;
    currentHue = nextHue > 360 ? 0 : nextHue;
    rTitle.style.color = color;
    rSubTitle.style.color = color;
    setTimeout(rainbowColor, rainbowTiming);
}
