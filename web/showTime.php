<?php
    // We will use 4 cookies for  current values, plus a hash to check for updates.
    // Bon appétit !
    $lifeTimeCookies = 86400 * 30; // time in seconds (30 days in this case)

    // fetch DEFAULT values stored in a file server-side
    $countFile_def = fopen('conf/sync/showTimeCounters_initialValues', 'r');
    $isConfetti_def = fgets($countFile_def);
    $changeColorText_def = fgets($countFile_def);
    $isAudio_def = fgets($countFile_def);
    $isAnimation_def = fgets($countFile_def);
    fclose($countFile_def);

    // Read hash data
    $hasDataChanged = false;
    if (!isset($_COOKIE["hash_data"]))
    {
        setcookie("hash_data", 0, time() + $lifeTimeCookies, "/");
        $hasDataChanged = true;
    }

    // Before doing anything, check if dataFile has been updated.
    // Compare the hash with the previous value (in cookies) 
    // If so, reset the counter.
    //$oldHash = trim($_COOKIE['hash_data']);
    $oldHash = ($_COOKIE && $_COOKIE["hash_data"]) ? trim($_COOKIE["hash_data"]): '';

 
    $newHash = trim(md5_file('conf/sync/dataToShow'));
    if ((true == $hasDataChanged) || ($oldHash != $newHash) || 
        (!isset($_COOKIE["confetti"]) || (!isset($_COOKIE["changeColor"])) || 
        (!isset($_COOKIE["soundAtStart"])) || (!isset($_COOKIE["animationAtStart"]))))
    {
        $hasDataChanged = true;
        // And rewrite hash
        setcookie("hash_data", $newHash, time() + $lifeTimeCookies, "/");
    }
?>

<!DOCTYPE html>
<html lang="fr">
<meta charset="UTF-8">

<meta http-equiv="refresh" content="60">

<head>
    <script src="../js/main.js"></script>
    <script src="../js/confetti.js"></script>
    <link rel="shortcut icon" type="image/jpg" href="img/favicon.png"/>
    <link rel="stylesheet" href="css/showTime.css">
    <title>Show time!</title>

</head>

<body>
    <?php 

        /* The values below are in minutes */
        $confettiFrequency = 15;
        $durationChangeColorText = 15; /* Change color of the text *during* ... minutes */
        $audioFrequency = 525600; /* Do it only once (or every year) */
        $animationFrequency = 525600; /* Do it only once (or every year) */

        function updateCookieValue($valueToUpdate, $threshold) {
            // Same thing as updateValue() (in a file) but without PHP_EOL
            if (strpos($valueToUpdate, "doTheAnimation") !== false)
            {
                /* At this point, run the animation (sound, confetti, ...) */
                return 0;
            }
            elseif (is_numeric(trim($valueToUpdate))) {
                /* it is a number: read its value and increment it */
                if (trim($valueToUpdate) >= $threshold)
                {
                    return "doTheAnimation";
                }
                else
                {
                    return (trim($valueToUpdate) + 1);
                }
            }
            else
            {
                /* Keep the value. Do nothing */
                return $valueToUpdate;
            }
        }

        // Get values from the files (updated or not)
        $dataFile = fopen('conf/sync/dataToShow', 'r');
        $manager = fgets($dataFile);
        $managerPic = fgets($dataFile);
        $client = fgets($dataFile);
        $consultant = fgets($dataFile);
        $consultantPic = fgets($dataFile);
        $hr = fgets($dataFile);
        fclose($dataFile);

        if ((true == $hasDataChanged) || 
            !((isset($_COOKIE["confetti"])) && (isset($_COOKIE["changeColor"])) && 
             (isset($_COOKIE["soundAtStart"])) && (isset($_COOKIE["animationAtStart"]))))
        {
            $isConfetti = $isConfetti_def;
            $changeColorText = $changeColorText_def;
            $isAudio = $isAudio_def;
            $isAnimation = $isAnimation_def;
            setcookie("confetti", trim($isConfetti_def), time() + $lifeTimeCookies, "/");
            setcookie("changeColor", trim($changeColorText_def), time() + $lifeTimeCookies, "/");
            setcookie("soundAtStart", trim($isAudio_def), time() + $lifeTimeCookies, "/");
            setcookie("animationAtStart", trim($isAnimation_def), time() + $lifeTimeCookies, "/");
        }
        else
        {
            $isConfetti = trim($_COOKIE["confetti"]);
            $changeColorText = trim($_COOKIE["changeColor"]);
            $isAudio = trim($_COOKIE["soundAtStart"]);
            $isAnimation = trim($_COOKIE["animationAtStart"]);
        }

        if (strpos($isConfetti, "doTheAnimation") !== false)
        {
            // Show fireworks (if unchecked, $isConfetti = "nope")
            echo("<script> doGreatFireworks(); </script>");
        }
        $isConfetti = updateCookieValue($isConfetti, $confettiFrequency);

        if (strpos($isAudio, "doTheAnimation") !== false)
        {
            // Play music at reload (if unchecked, $isAudio = "nope")
            echo("<!-- For a better user experience (ahem), please enable autoplay :) -->");
            echo("<audio autoplay><source src='mp3/soundAtStart.mp3' type='audio/mpeg'></audio>");
        }
        $isAudio = updateCookieValue($isAudio, $audioFrequency);

        if (strpos($isAnimation, "doTheAnimation") !== false)
        {
            // Play animation at reload (if unchecked, $isAnimation = "nope")
            echo("<link rel='stylesheet' href='css/crazyAnimations.css'>");
        }
        $isAnimation = updateCookieValue($isAnimation, $animationFrequency);
    ?>

    <div class="header">
        <h1 class="rainbowColor shakeIt">Nouveau projet&nbsp;!</h1>
        <img class="logo" src="img/Adentis_new_logo.png" alt="Logo Adentis">
        <p class="rainbowColor shakeIt"> <i><script>congratsText();</script></i></p>
    </div>

    <hr />

    <div class='contract'>
        <div class="text shakeIt">
            <span class="manager"><?php echo($manager); ?></span> a décroché un contrat 
            dans l'entreprise <span class="client"><?php echo($client);?></span>!
        </div>
        <?php
            if (strlen($managerPic) > 4)
            {
                // Add picture of manager (no alt attribute to handle absence of image)
                echo("<img class='trombiManager shakeIt' src='". $managerPic ."'>");
            }
        ?>
    </div>

    <div class='engineer'>
        <div class="text shakeIt">
            <span class="consultant"><?php echo($consultant); ?></span> travaillera sur ce projet.<br />
            <span class="hr"><?php echo($hr); ?></span> était en charge du recrutement&nbsp;!
        </div>
        <?php
            $changeColorText = trim($changeColorText);
            if ((is_numeric($changeColorText) !== false) && ($changeColorText < $durationChangeColorText))
            {
                /* Do the color thing */
                echo("<script>rainbowColor(); </script>");
                $changeColorText = $changeColorText + 1;
            }

            if (strlen($consultantPic) > 4)
            {
                // Add picture of consultant (no alt attribute to handle lack of image or incorrect file)
                echo("<img class='trombiConsultant shakeIt' src='". $consultantPic ."'>");
            }

            /* The variables have been updated: update the cookies */
            setcookie("confetti", trim($isConfetti), time() + $lifeTimeCookies, "/");
            setcookie("changeColor", trim($changeColorText), time() + $lifeTimeCookies, "/");
            setcookie("soundAtStart", trim($isAudio), time() + $lifeTimeCookies, "/");
            setcookie("animationAtStart", trim($isAnimation), time() + $lifeTimeCookies, "/");
        ?>
    </div>
</body>
