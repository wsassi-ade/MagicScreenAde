#! /bin/sh
cd ~/MagicScreenAde
git checkout -f
git pull
sudo cp -r web/* /var/www/html
cp setup-rpi/kiosk /home/pi/
cp -r setup-rpi/scriptSync/ /home/pi/
chmod 777 /home/pi/scriptSync/*
sudo cp ~/Music/songAtStart.mp3 /var/www/html/mp3/soundAtStart.mp3
sudo chmod -R 777 /var/www/html/*
amixer set Master 100%  # Sound at max power!
