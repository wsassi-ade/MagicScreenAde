# Magic screen
*An Adentis project*

## Why this project?
When a consultant is in a new project, celebrate that in the best way possible.

## Setup
These steps will guide you from a functional Raspberry Pi to a server running great things!  

### Configure the device
After having installed Raspberry OS on your device (or your computer), there are a few things to do:  
If you have a black border on your screen, edit (as admin) the file `/boot/config.txt` and uncomment the line `#disable_overscan=1` (remove the hash on the first character).

Now, with `sudo raspi-config`:
- In System options: Boot on Desktop with autologin (will be changed in the end of the config)
- Change hostname (`rpi-adentis` is used in this tutorial)
- Change password
- (you can keep pi as the user. If you change it, you should replace occurences of pi with your user name).
- In Interface: Enable SSH and VNC (may be simpler when configuring the device)
- In Advanced Config: Expand filesystem
Quit the utility and reboot.  

_Note_: if you are not a lot into command-line, you can type `startx` to start the GUI.  

### Optional: forcing the output into HDMI
To not reboot your device when screen is not attached, you can force X output.  
Edit the config file (`sudo nano /boot/config.txt`) and add the following lines (you can also uncomment them in the 40 first lines of the file):  
```
hdmi_force_hotplug=1
hdmi_drive=2
```

Upon the next reboot, HDMI will always be enabled (with audio output).  

### Setting up Apache server
You will need to set up the PHP server. To do so, just install what you need:  
```sh
sudo apt update
sudo apt install -y php libapache2-mod-php apache2 unclutter chromium-browser matchbox-window-manager xautomation
cd /var/www/html/
sudo mkdir conf
sudo mkdir conf/sync
sudo touch conf/counterVisitsLocal
sudo chmod 777 conf/*
```

### Cloning the project
On your Raspi, clone this repository into home folder (or copy files into a folder named MagicScreenAde):
```sh
cd ~
git clone https://gitlab.com/wsassi-ade/MagicScreenAde.git
cd MagicScreenAde
```

After that, copy necessary files where it is needed: 
```sh
sudo rm /var/www/html/index.html
sudo cp -r web/* /var/www/html
cp setup-rpi/kiosk /home/pi/
cp -r setup-rpi/scriptSync /home/pi/
chmod +x /home/pi/kiosk
chmod 777 /home/pi/scriptSync/*
```

Finally, start the kiosk (the webpage on full screen) at startup by appending `xinit /home/pi/kiosk -- vt$(fgconsole)` on `.bashrc`: 
```sh
echo "xinit /home/pi/kiosk -- vt\$(fgconsole)" | tee -a ~/.bashrc
```

After a reboot, the form is now accessible on the local network (no external connection!) at this address: http://rpi-adentis:80/  
(if you changed the hostname to something else, replace `rpi-adentis` with the hostname you set or the local IP address).

## First time configuration: more things to do
So you launched the lines of code and everything was fine.  
You ran the raspi-config tool to finish configuring your device.  
But now, there are some things to do to finish.  

### Removing the translation toolbar
Chromium really wants to help you, even if you don't want it! So, despite the flag `--disable-translation`, you may see a toolbar popping up to help you translate the page.  
Go to the settings and uncheck the option to offer an auto translation (as in version 95, it is called _"Offer to translate pages that aren't in a language you read"_ in Languages tab. You will need to expand Language to see it (first line)).  

### Change the celebration sound
For copyright reason, I can't put the Kaamelott horn on this repo. Instead, I took a [CC0 sound of applause](https://lasonotheque.org/detail-2363-applaudissements-1.html) which can be used without problems (thanks Dorian Clair).  
If you want to put your very own song (even a personnal recording but in MP3), copy your file into `/home/pi/Music` and rename it `songAtStart.mp3`.  
When running the script, it will be copied and you will hear _your_ song! 

## Updating the project
The world changes so as the website. You can update it easily thanks to the script in `setup-rpi/fetchLastCommit`.  
As the repo can be private (or not if you read these lines on GitLab), you may want to log in. 
Because the project should be over, you should not need to do this.  

# Synchronize data with more devices
You can use a cloud provider to sync data through several devices. Because data at stake is not quite high (< 1kB), no need to allocate too much memory.  
To be able to synchronize data through several devices, we will use `rclone` which supports OneDrive folders!  

Configuration is a bit tricky but will be done only once. Follow [this link](https://dariusz.wieckiewicz.org/en/microsoft-365-sharepoint-onedrive-free-backup-raspberry-pi-rclone/#rclone) to do it (no need to follow steps before _rclone_ paragraph and from _SharePoint_ paragraph on).  
Here is a quick summary of things to do. Rclone official documentation is [here](https://rclone.org/onedrive/).  
Just before, you should configure your Raspberry to start on Desktop and with autologin (via `raspi-config`). It will be useful when entering your credentials on Chromium. 

```sh
# Install rclone to sync files with remote folder. Note: we can not use aptitude because version present is too old and won't work with rclonesync.
curl https://rclone.org/install.sh | sudo bash
# Create working directory for rclonesync (allow bilateral sync)
mkdir /home/pi/.rclonesyncwd
# Run configuration (you will need a X server enabled to log in: VNC makes that possible)
rclone config
# New remote
n
# Name
o365onedrive
# Select Microsoft OneDrive (or type onedrive or 27 as in version 1.54)
onedrive
# Press enter twice (no ID or password)
# If it asks you a country, type global / 1 (even for French infrastructures)
1
# No advanced config
n
# I want auto config
y
```
Now, you will need to enter your credentials into your browser (if you are using GUI, the window will be opened automatically. Otherwise, with VNC, open Chromium and go to `http://127.0.0.1:53862/auth` (the link will start like this: it may differ) and enter your credentials.  

When done, you can go back to your console and type `1` for OneDrive Personal.  
Read and confirm twice `y` to validate your config.  
When it is over, press `q` to quit the wizard.  

Need to check? Type `rclone ls o365onedrive:` and you should be able to see the files in your OneDrive.  

To start sync, follow these steps: 
```sh
# First sync (from server to machine)
/home/pi/scriptSync/rclonesync /var/www/html/conf/sync o365onedrive:conf/sync --first-sync
# Append crontab file to schedule sync every two minutes
echo "*/2 * * * * /home/pi/scriptSync/syncFiles" | sudo tee -a /var/spool/cron/crontabs/pi
# keep one blank line in the end of file!
echo " " | sudo tee -a /var/spool/cron/crontabs/pi
# After a reboot, the task will be scheduled
```
Now, you can reconfigure your Raspberry to start on the console with autologin (as before: option `B2`) to display the page at startup with `sudo raspi-config`.  
Then, reboot and your device is ready to go :) !

## Troubleshooting
> When completing the form, I have `403 Forbidden`  

There might be a problem with Apache. First, troubleshoot what's wrong with it by running this command on Terminal: 
`sudo systemctl status apache2.service`.  
You will have a hint about the issue. If there is another service using port 80, you will need to check which is it with `sudo lsof -i:80`.  
To ~kill the culprit~ terminate the conflicting service (example here: `lighttpd` -- replace the service with the one you have), do `sudo systemctl stop lighttpd.service` and restart Apache with `sudo systemctl start apache2.service`.  

Even more radical: just run `sudo systemctl disable lighttpd.service` and you will never hear about it again.  
  
That should be good.

> I can't see the form (I have a Apache page telling me "it works")!

On first boot, you may have an Apache page telling you that the server works. Good but I want to go to the form.  
To do so, just delete the `index.html` page:
```
sudo rm /var/www/html/index.html
```
Now, the default page is _your_ page!  

> The output resolution is really low: I can see the pixels from the space!

If no device is attached when the Raspberry boots, the HDMI output will be enabled anyway (by default, it should display a blank screen).  
However, the resolution to output is unknown so the fallback is to use the default resolution: 640x480 (a pity if you have a FHD screen).  
You have two solutions: 
- The easiest: reboot the device *after* connecting it to a screen. And if you boot your device, plug the screen and power it on. 
- If you have only one screen dedicated for your device: to force the resolution to a value, you can set the right mode on the line `hdmi_mode` in `/boot/config.txt`. See how to set it [here](https://www.raspberrypi.com/documentation/computers/config_txt.html#hdmi-mode). If you have a 4k monitor but not a Raspberry Pi 4, you can't select a 4K resolution (maximum supported: 1920x1200).

> I can't sync my files!

There are too much changes (a lot of added/removed files) and sync is blocked for safety measures (should not happen).  
You can **force** sync by doing `/home/pi/scriptSync/rclonesync /var/www/html/conf/sync o365onedrive:conf/sync --force`.  
If prompted, you can also rerun the sync for the first time with the argument `--first-sync`

If it still fails (with "operation not permitted"), check the access of files in /var/www/html/conf/sync. They must look like this (when you do `ls -alg /var/www/html/conf/sync`):
```sh
pi@rpi-adentis:~ $ ls -alg /var/www/html/conf/sync
total 24
drwxrwxrwx 2 pi   4096 Nov 17 18:12 .
drwxrwxrwx 3 root 4096 Nov 17 10:33 ..
-rwxrwxrwx 1 pi      3 Nov 17 18:15 counterVisitsGlobal
-rwxrwxrwx 1 pi     27 Nov 17 18:15 dataToShow
-rwxrwxrwx 1 pi    796 Nov 17 12:23 readme.txt
-rwxrwxrwx 1 pi     47 Nov 17 18:15 showTimeCounters_initialValues
```
If there are more dashes in the left part, run this command to get full access:  
```sh
sudo chmod 777 /var/www/html/conf/*
sudo chmod 777 /var/www/html/conf/sync/*
```

> I want to add another Wi-Fi connection

You have two solutions:
- Either you reboot with Desktop (with `sudo raspi-config`), then after a reboot, you add the network via the GUI (on the right of the toolbar). Don't forget to reboot on Console autologin with raspi-config.  
- Or you can do the modifications directly via SSH with `sudo nano /etc/wpa_supplicant/wpa_supplicant.conf`.  

For the latter (the best solution, I think), the file you will edit should look like this:  
```sh
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=FR

network={
        ssid="BoiteB-1234"
        psk="Too_much_hexadecimal_characters"
        key_mgmt=WPA-PSK
}

network={
        ssid="BoiteVivante-1234"
        psk="Another_great_password"
        key_mgmt=WPA-PSK
}

network={
        ssid="BoiteLibre-1234"
        psk="A_good_password"
        key_mgmt=WPA-PSK
}
```
To add a connection, just copy-paste the last six lines and replace the information with the ones you want. If the Wi-Fi access is protected with WPA2, you can keep `WPA-PSK` as key_mgmt.   

> I want to add custom sentences below "Nouveau projet"

You can! Let your imagination talk by editing the file in `/var/www/html/js/main.js`!  
Just copy-paste the last line starting with `congratsText`, increment the number between brackets, and increment the value of nbTxt.  
An example: `congratsText[10] = "Hip, hip, hip ! Hourra !";`.  



> I want to enter the interface on TV Via wifi connection 

1- configuring wifi connection (like in this section "> I want to add another Wi-Fi connection")
2- A static IP

You are be able to work with the RPi without external keyboard or display, you want to be ssh into it. The best way is to make sure it'll always have a static IP on your network.

Doing so is simple. Open the /etc/network/interfaces file again and add the following changes:

Change iface wlan0 inet dhcp into iface wlan0 inet static. This changes the wlan0 interface from DHCP to static.

Add the following lines before the wpa-conf line:

address 192.168.1.155 # Static IP you want 
netmask 255.255.255.0 
gateway 192.168.1.1   # IP of your router

The Raspberry Pi will still be able to connect to the internet.

With these changes you'll be able to always connect to your Raspberry Pi over your wireless network via ssh at the same, static IP. This means you can disconnect keyboard, mouse and display and have it plugged in a wall socket, anywhere, taking almost no space.

As an overview, my interfaces- and wpa_supplicant-files:

# /etc/network/interfaces

auto wlan0

iface lo inet loopback
iface eth0 inet dhcp

allow-hotplug wlan0
iface wlan0 inet static
address 192.168.1.155
netmask 255.255.255.0
gateway 192.168.1.1
wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
iface default inet dhcp


# /etc/wpa_supplicant/wpa_supplicant.conf

ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
ssid="NYO_WWWP"
psk="topsecret"
proto=RSN
key_mgmt=WPA-PSK
pairwise=CCMP
auth_alg=OPEN
}



# File Tree
In this paragraph, the role of each file will be explained.  

```                 
.                               --> Root of your file. In Raspberry, it is situated in /var/www/html  
├── conf                            --> Folder created during the setup. !!! Folder and files must be created and freely accessible (chmod 777) !!!  
│   ├── sync                            --> Folder synced with OneDrive 
│   │   ├── counterVisitsGlobal             --> Global counter of visits (how many times the form is completed everywhere). Nice to know.  
│   │   ├── dataToShow                      --> Where variable data is stored (what the user inputs).  
│   │   └── showTimeCounters_initialValues  --> Which animations to do (useful when cookies are cleared or for the first connection client-side)
│   └── counterVisitsLocal              --> Local counter of visits (how many times the form is completed in this network). Nice to know.   
├── css                         --> Folder containing style sheets (from GitLab). Nothing is different from GitLab.  
│   ├── crazyAnimations.css         --> What to do (HTML5) when we enable animations  
│   ├── form.css                    --> Style for forms (with a bit of animation)  
│   └── showTime.css                --> Style for the showTime page. Completely adjusted depending on screen size (in landscape mode)  
├── img                         --> Images folder. Only images with CC0 license, please!  
│   ├── Adentis_big.png             --> Adentis logo (taken from Adentis website)  
│   ├── favicon.png                 --> Icon displayed on the left of a tab.  
│   ├── fireworks_1920.jpg          --> When a form is submitted, celebrate this with fireworks  
│   └── handshake_1920.jpg          --> When you complete the form, it means you made a contract. Choose an appropriate image.  
├── js                          --> Javascript files for fancy animations  
│   ├── confetti.js                 --> From npm. No need to download this script because it is downloaded internally!  
│   └── main.js                     --> Our handmade scripts!  
├── mp3                         --> When calling an orchestra, folder containing the songs played (one in this case)  
│   └── soundAtStart.mp3            --> Name should not change. By default, a CC0 sound too (copyright laws). Can be replaced by what you want.  
├── confirmation.php            --> When you sent a form, display a confirmation page. It will set up conf/ files and reset counters  
├── index.php                   --> Homepage. This is the form.  
└── showTime.php                --> Page shown on screen to display success of your colleagues.  
```

------
